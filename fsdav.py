# Copyright (c) 2005.-2006. Ivan Voras <ivoras@gmail.com>
# Released under the Artistic License

from member import Member
from collection import *
import os.path, os
import md5
import dircache
from util import *
import mimetypes


class FileMember(Member):

    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
        self.name = name
        self.fsname = parent.fsname + name # e.g. '/var/www/mysite/some.txt'
        self.virname = parent.virname + name # e.g. '/mysite/some.txt'
        self.type = Member.M_MEMBER

        assert(type(self.name) == unicode)
        assert(type(self.fsname) == unicode)
        assert(type(self.virname) == unicode)

    def __str__(self):
        return "%s -> %s" % (self.virname, self.fsname)

    def getProperties(self):
        """Return dictionary with WebDAV properties. Values shold be
        formatted according to the WebDAV specs."""
        st = os.stat(self.fsname)
        p = {}
        p['creationdate'] = unixdate2iso8601(st.st_ctime)
        p['getlastmodified'] = unixdate2httpdate(st.st_mtime)
        if self.type == Member.M_COLLECTION:
            p['displayname'] = self.name.strip(u"/").split('/')[-1]
        else:
            p['displayname'] = self.name
        p['getetag'] = md5.new(self.fsname.encode("utf-8")).hexdigest()
        #p['resourcetype'] = None
        if self.type == Member.M_MEMBER:
            p['getcontentlength'] = st.st_size
            p['getcontenttype'], z = mimetypes.guess_type(self.name)
            p['getcontentlanguage'] = None
        if self.name[0] == ".":
            p['ishidden'] = 1
        if not os.access(self.fsname, os.W_OK):
            p['isreadonly'] = 1
        if self.name == '/':
            p['isroot'] = 1
        return p


    def sendData(self, wfile):
        """Send the file to the client. Literally."""
        st = os.stat(self.fsname)
        f = file(self.fsname, 'rb')
        writ = 0
        while writ < st.st_size:
            buf = f.read(65536)

            if len(buf) == 0: # eof?
                break

            writ += len(buf)
            wfile.write(buf)
        f.close()



class DirCollection(FileMember, Collection):

    COLLECTION_MIME_TYPE = 'httpd/unix-directory'

    def __init__(self, fsdir, virdir, parent=None):
        if not os.path.exists(fsdir):
            raise "Local directory (fsdir) not found: " + fsdir
        assert( type(fsdir) == unicode )
        assert( type(virdir) == unicode )
        self.fsname = fsdir
        self.name = virdir

        if self.fsname[-1] != os.sep:
            if self.fsname[-1] == '/': # fixup win/dos/mac separators
                self.fsname = self.fsname[:-1] + os.sep
            else:
                self.fsname += os.sep

        self.virname = virdir
        if self.virname[-1] != '/':
            self.virname += '/'

        self.parent = parent
        self.type = Member.M_COLLECTION


    def getProperties(self):
        p = FileMember.getProperties(self) # inherit file properties
        p['iscollection'] = 1
        p['getcontenttype'] = DirCollection.COLLECTION_MIME_TYPE
        return p


    def getMembers(self):
        """Get immediate members of this collection."""
        dircache.reset() # workaround to make sure we also get very recently created files
        l = dircache.listdir(self.fsname)[:] # obtain a copy of dirlist
        dircache.annotate(self.fsname, l)
        r = []
        for f in l:
            if f[-1] != '/':
                m = FileMember(f, self) # Member is a file
            else:
                m = DirCollection(self.fsname + f, self.virname + f, self) # Member is a collection
            r.append(m)
        return r


    def findMember(self, name):
        """Search for a particular member."""
        dircache.reset() # workaround to make sure we also get very recently created files
        l = dircache.listdir(self.fsname)[:] # obtain a copy of dirlist
        dircache.annotate(self.fsname, l)
        #print "%s - %s, find %s" % (self.fsname, repr(l), name)

        if name in l:
            if name[-1] != '/':
                return FileMember(name, self)
            else:
                return DirCollection(self.fsname + name, self.virname + name, self)
        elif name[-1] != '/':
            name += '/'
            if name in l:
                return DirCollection(self.fsname + name, self.virname + name, self)


    def sendData(self, wfile):
        """Send "file" to the client. Since this is a directory, build some arbitrary HTML."""
        memb = self.getMembers()
        data = '<html><head><title>%s</title></head><body>' % self.virname
        data += '<table><tr><th>Name</th><th>Size</th><th>Timestamp</th></tr>'
        for m in memb:
            p = m.getProperties()
            if 'getcontentlength' in p:
                p['size'] = int(p['getcontentlength'])
                p['timestamp'] = p['getlastmodified']
            else:
                p['size'] = 0
                p['timestamp'] = '-DIR-'
            data += '<tr><td>%s</td><td>%d</td><td>%s</td></tr>' % (p['displayname'], p['size'], p['timestamp'])
        data += '</table></body></html>'
        wfile.write(data)


    def recvMember(self, rfile, name, size, req):
        """Receive (save) a member file"""
        fname = os.path.join(self.fsname, name)
        f = file(fname, 'wb')
        writ = 0
        bs = 65536
        while True:
            if size != -1 and (bs > size-writ):
                bs = size-writ
            buf = rfile.read(bs)
            if len(buf) == 0:
                break
            f.write(buf)
            writ += len(buf)
            if size != -1 and writ >= size:
                break
        f.close()

    def createSubCollection (self, name):
        dname = os.path.join(self.fsname, name)

        if os.path.exists(dname):
            raise CollectionExistsError

        print "creating new folder '%s'" % dname
        try:
            os.mkdir(dname)
        except OSError, e:
            raise CollectionError, str(e)


