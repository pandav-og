# A sane XML-to-objects parser
# TODO: error & better malformed xml handling
# (c) 2005. Ivan Voras
import sys
import re

from xml.dom import minidom

class Tag:

    def __init__(self, name, attrs, data='', parser=None):
        self.d = {}
        self.name = name
        self.attrs = attrs
        if type(self.attrs) == type(''):
            self.attrs = splitattrs(self.attrs)
        for a in self.attrs:
            if a.startswith('xmlns'):
                nsname = a[6:]
                parser.namespaces[nsname] = self.attrs[a]
        self.rawname = self.name

        p = name.find(':')
        if p > 0:
            nsname = name[0:p]
            if nsname in parser.namespaces:
                self.ns = parser.namespaces[nsname]
                self.name = self.rawname[p+1:]
        else:
            self.ns = ''
        #print self.rawname, '->', self.name, self.ns
        self.data = data

    # Emulate dictionary d
    def __len__(self):
        return len(self.d)

    def __getitem__(self, key):
        return self.d[key]

    def __setitem__(self, key, value):
        self.d[key] = value

    def __delitem__(self, key):
        del self.d[key]

    def __iter__(self):
        return self.d.iterkeys()

    def __contains__(self, key):
        return key in self.d

    def prettyPrint (self, indent=0):
        s = "  " * indent
        if self.attrs:
            s += u'<%s %s> %s ' % (self.name, self.attrs, self.data)
        else:
            s += u'<%s> %s ' % (self.name, self.data)

        s += "\n"
        for k in self.d:
            if type(self.d[k]) == type(self):
                s += " " + self.d[k].prettyPrint(indent + 1)
            else:
                for e in self.d[k]:
                    s += "-" + e.prettyPrint(indent + 1)
        return s

    def __str__(self):
        """Returns unicode semi human-readable representation of the structure"""
        if self.attrs:
            s = u'<%s %s> %s ' % (self.name, self.attrs, self.data)
        else:
            s = u'<%s> %s ' % (self.name, self.data)

        for k in self.d:
            if type(self.d[k]) == type(self):
                s += u'|%s: %s|' % (k, str(self.d[k]))
            else:
                s += u'|' + u','.join([str(x) for x in self.d[k]]) + u'|'
        return s


    def addChild(self, tag):
        """Adds a child to self. tag must be instance of Tag"""
        if tag.name in self.d:
            if type(self.d[tag.name]) == type(self): # If there are multiple sibiling tags with same name, form a list :)
                self.d[tag.name] = [self.d[tag.name]]
            self.d[tag.name].append(tag)
        else:
            self.d[tag.name] = tag
        return tag


    def toUnicode(self, fromencoding, recurse=True):
        """Converts data & attribute data to unicode from specified encoding"""
        if type(self.data) == type(''):
            self.data = self.data.decode(fromencoding, 'replace')
        for a in self.attrs:
            if type(self.attrs[a] == type('')):
                self.attrs[a] = self.attrs[a].decode(fromencoding, 'replace')
        if recurse:
            for k in self.d:
                if type(self.d[k]) == type(self):
                    self.d[k].toUnicode(fromencoding, recurse)



class XMLDict_Parser:

    def __init__(self, xml):
        self.xml = xml
        self.p = 0
        self.encoding = sys.getdefaultencoding()
        self.namespaces = {}

    def parseNode (self, parent, node):

        assert(type(parent) == type(Tag("", "", "")))

        nodeText = ""
        for n in node.childNodes:
            if n.nodeType == n.TEXT_NODE:
                nodeText = nodeText + n.nodeValue.strip()
            elif n.nodeType == n.COMMENT_NODE:
                sys.stderr.write("Note: ignoring comment\n")
            elif n.nodeType == n.ELEMENT_NODE:

                # ignore tags that are not in DAV: namespace;
                # but, as exception, allow tags that are in no namespace
                if n.namespaceURI != None and n.namespaceURI != "DAV:":
                    sys.stderr.write("Note: ignoring non-DAV element (%s%s)\n" %
                        (n.namespaceURI, n.localName) )
                    continue

                if len(nodeText) > 0:
                    sys.stderr.write("Warning: mixed content (tags/data) ?!\n")
                newTag = Tag(n.localName, "", parser=self)
                parent.addChild( newTag )

                # add attributes
                for attr in n.attributes.keys():
                    newTag.attrs[attr] = n.attributes[attr].value

                # add child nodes
                self.parseNode(newTag, n)

        parent.data = nodeText

    def builddict(self):
        """Builds a nested-dictionary-like structure from the xml. This method
        picks up tags on the main level and calls processTag() for nested tags."""

        xmldoc = minidom.parseString(self.xml)
        d = Tag('<root>', '')

        self.parseNode(d, xmldoc)

        return d


def splitattrs(att):
    """Extracts name="value" pairs from string; returns them as dictionary"""
    d = {}
    for m in re.findall('([a-zA-Z_][a-zA-Z_:0-9]*?)="(.+?)"', att):
        d[m[0]] = m[1]
    return d


def builddict(xml):
    """Wrapper function for straightforward parsing"""
    p = XMLDict_Parser(xml)
    return p.builddict()



import unittest

class XMLTest(unittest.TestCase):
    def testOneTagWithContent1(self):
        """one tag with content"""
        d = builddict("<tag1>text</tag1>")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["tag1"].name, "tag1")
        self.assertEqual(d["tag1"].attrs, {})
        self.assertEqual(d["tag1"].data, "text")

    def testOneEmptyTag1(self):
        "one tag without content (short notation)"
        d = builddict("<tag1/>")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["tag1"].name, "tag1")
        self.assertEqual(d["tag1"].attrs, {})
        self.assertEqual(d["tag1"].data, "")

    def testOneEmptyTag2(self):
        "one tag without content (short notation); with in-tag whitespace"
        d = builddict("<tag1 />")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["tag1"].name, "tag1")
        self.assertEqual(d["tag1"].attrs, {})
        self.assertEqual(d["tag1"].data, "")

    def testTwoNestedTagsWithContent1(self):
        "two nested tags, with content"
        d = builddict("<group><user>joe</user><user>nick</user><user>john</user></group>")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["group"].name, "group")
        self.assertEqual(d["group"].attrs, {})
        self.assertEqual(d["group"].data, "")

        self.assertEqual(type(d["group"]["user"]), type([]))
        self.assertEqual(len(d["group"]["user"]), 3)
        self.assertEqual(d["group"]["user"][0].name, "user")
        self.assertEqual(d["group"]["user"][0].attrs, {})
        self.assertEqual(d["group"]["user"][0].data, "joe")
        self.assertEqual(d["group"]["user"][1].name, "user")
        self.assertEqual(d["group"]["user"][1].attrs, {})
        self.assertEqual(d["group"]["user"][1].data, "nick")
        self.assertEqual(d["group"]["user"][2].name, "user")
        self.assertEqual(d["group"]["user"][2].attrs, {})
        self.assertEqual(d["group"]["user"][2].data, "john")

    def testTwoNestedEmptyTags1(self):
        "two nested tags, short notation"
        d = builddict("<group><user/><user/><user/><user/></group>")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["group"].name, "group")
        self.assertEqual(d["group"].attrs, {})
        self.assertEqual(d["group"].data, "")

        self.assertEqual(type(d["group"]["user"]), type([]))
        self.assertEqual(len(d["group"]["user"]), 4)
        self.assertEqual(d["group"]["user"][0].name, "user")
        self.assertEqual(d["group"]["user"][0].attrs, {})
        self.assertEqual(d["group"]["user"][0].data, "")
        self.assertEqual(d["group"]["user"][1].name, "user")
        self.assertEqual(d["group"]["user"][1].attrs, {})
        self.assertEqual(d["group"]["user"][1].data, "")
        self.assertEqual(d["group"]["user"][2].name, "user")
        self.assertEqual(d["group"]["user"][2].attrs, {})
        self.assertEqual(d["group"]["user"][2].data, "")
        self.assertEqual(d["group"]["user"][3].name, "user")
        self.assertEqual(d["group"]["user"][3].attrs, {})
        self.assertEqual(d["group"]["user"][3].data, "")
        self.assertEqual(str(d["group"]["user"][0]), str(d["group"]["user"][1]))
        self.assertEqual(str(d["group"]["user"][0]), str(d["group"]["user"][2]))
        self.assertEqual(str(d["group"]["user"][0]), str(d["group"]["user"][3]))

    def testTwoNestedEmptyTags2(self):
        "two nested tags, short notation, different tag names"
        d = builddict("<users><joe/><nick/><john/></users>")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["users"].name, "users")
        self.assertEqual(d["users"].attrs, {})
        self.assertEqual(d["users"].data, "")

        self.assertEqual(len(d["users"]), 3)
        self.assertEqual(d["users"]["joe"].name, "joe")
        self.assertEqual(d["users"]["joe"].attrs, {})
        self.assertEqual(d["users"]["joe"].data, "")
        self.assertEqual(d["users"]["nick"].name, "nick")
        self.assertEqual(d["users"]["nick"].attrs, {})
        self.assertEqual(d["users"]["nick"].data, "")
        self.assertEqual(d["users"]["john"].name, "john")
        self.assertEqual(d["users"]["john"].attrs, {})
        self.assertEqual(d["users"]["john"].data, "")

    def testThreeNestedTags1(self):
        "three nested tags, one with short notation, one with content"
        d = builddict("<tag1><tag2/><tag3>cont3</tag3></tag1>")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["tag1"].name, "tag1")
        self.assertEqual(d["tag1"].attrs, {})
        self.assertEqual(d["tag1"].data, "")

        self.assertEqual(len(d["tag1"]), 2)
        self.assertEqual(d["tag1"]["tag2"].name, "tag2")
        self.assertEqual(d["tag1"]["tag2"].attrs, {})
        self.assertEqual(d["tag1"]["tag2"].data, "")
        self.assertEqual(d["tag1"]["tag3"].name, "tag3")
        self.assertEqual(d["tag1"]["tag3"].attrs, {})
        self.assertEqual(d["tag1"]["tag3"].data, "cont3")


    def testThreeNestedTags2(self):
        "three nested tags, one with short notation and in-tag whitespace, one with content"
        d = builddict("<tag1><tag2 /><tag3>cont3</tag3></tag1>")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["tag1"].name, "tag1")
        self.assertEqual(d["tag1"].attrs, {})
        self.assertEqual(d["tag1"].data, "")

        self.assertEqual(len(d["tag1"]), 2)
        self.assertEqual(d["tag1"]["tag2"].name, "tag2")
        self.assertEqual(d["tag1"]["tag2"].attrs, {})
        self.assertEqual(d["tag1"]["tag2"].data, "")
        self.assertEqual(d["tag1"]["tag3"].name, "tag3")
        self.assertEqual(d["tag1"]["tag3"].attrs, {})
        self.assertEqual(d["tag1"]["tag3"].data, "cont3")


    def testOneTagWithAttr1(self):
        "tag with attribute and single quotes"
        d = builddict("<tag1 someattr='mycontent'>text</tag1>")
        self.assertEqual(len(d), 1)
        self.assertEqual(d["tag1"].name, "tag1")
        self.assertEqual(d["tag1"].attrs, {"someattr" : "mycontent"})
        self.assertEqual(d["tag1"].data, "text")

    def testOneTagWithAttr2(self):
        "tag with attribute and double quotes"
        d = builddict('<tag1 someattr="mycontent">text</tag1>')
        self.assertEqual(len(d), 1)
        self.assertEqual(d["tag1"].name, "tag1")
        self.assertEqual(d["tag1"].attrs, {"someattr" : "mycontent"})
        self.assertEqual(d["tag1"].data, "text")


    def testRealContent1(self):
        "short DAV XML"
        d = builddict("""<propfind xmlns="DAV:"><prop>
<getlastmodified xmlns="DAV:" />
</prop></propfind>""")

        self.assertEqual(len(d), 1)
        self.assertEqual(d["propfind"].name, "propfind")
        self.assertEqual(d["propfind"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"].data, "")

        self.assertEqual(len(d["propfind"]), 1)
        self.assertEqual(d["propfind"]["prop"].name, "prop")
        self.assertEqual(d["propfind"]["prop"].attrs, {})
        self.assertEqual(d["propfind"]["prop"].data, "")

        self.assertEqual(len(d["propfind"]["prop"]), 1)
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].name, "getlastmodified")
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].data, "")


    def testRealContent2(self):
        "longer DAV XML"
        d = builddict("""<?xml version="1.0" encoding="utf-8"?>
<propfind xmlns="DAV:"><prop>
<getlastmodified xmlns="DAV:"/>
<creationdate xmlns="DAV:"/>
<resourcetype xmlns="DAV:"/>
<getcontenttype xmlns="DAV:"/>
<getcontentlength xmlns="DAV:"/>
</prop></propfind>""")

        self.assertEqual(len(d), 1)
        self.assertEqual(d["propfind"].name, "propfind")
        self.assertEqual(d["propfind"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"].data, "")

        self.assertEqual(len(d["propfind"]), 1)
        self.assertEqual(d["propfind"]["prop"].name, "prop")
        self.assertEqual(d["propfind"]["prop"].attrs, {})
        self.assertEqual(d["propfind"]["prop"].data, "")

        self.assertEqual(len(d["propfind"]["prop"]), 5)
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].name, "getlastmodified")
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].data, "")
        self.assertEqual(d["propfind"]["prop"]["creationdate"].name, "creationdate")
        self.assertEqual(d["propfind"]["prop"]["creationdate"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"]["prop"]["creationdate"].data, "")
        self.assertEqual(d["propfind"]["prop"]["resourcetype"].name, "resourcetype")
        self.assertEqual(d["propfind"]["prop"]["resourcetype"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"]["prop"]["resourcetype"].data, "")
        self.assertEqual(d["propfind"]["prop"]["getcontenttype"].name, "getcontenttype")
        self.assertEqual(d["propfind"]["prop"]["getcontenttype"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"]["prop"]["getcontenttype"].data, "")
        self.assertEqual(d["propfind"]["prop"]["getcontentlength"].name, "getcontentlength")
        self.assertEqual(d["propfind"]["prop"]["getcontentlength"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"]["prop"]["getcontentlength"].data, "")


    def testRealContent3(self):
        "short DAV XML with namespace"
        d = builddict("""<?xml version="1.0" encoding="utf-8" ?>
<D:propfind xmlns:D="DAV:"><D:prop>
<D:getlastmodified/>
</D:prop></D:propfind>""")

        self.assertEqual(len(d), 1)
        self.assertEqual(d["propfind"].name, "propfind")
        self.assertEqual(d["propfind"].attrs, {"xmlns:D" : "DAV:"})
        self.assertEqual(d["propfind"].data, "")

        self.assertEqual(len(d["propfind"]), 1)
        self.assertEqual(d["propfind"]["prop"].name, "prop")
        self.assertEqual(d["propfind"]["prop"].attrs, {})
        self.assertEqual(d["propfind"]["prop"].data, "")

        self.assertEqual(len(d["propfind"]["prop"]), 1)
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].name, "getlastmodified")
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].attrs, {})
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].data, "")


    def testMixedNamespaces(self):
        "tags from two namespaces"
        d = builddict("""<MyD:propfind xmlns:MyD="DAV:" xmlns="http://example.com/blah/">
<someothertag>aklsj</someothertag>
<MyD:prop>
<getlastmodified xmlns="DAV:" />
<getcolor />
</MyD:prop></MyD:propfind>""")

        self.assertEqual(len(d), 1)
        self.assertEqual(d["propfind"].name, "propfind")
        self.assertEqual(d["propfind"].attrs, {"xmlns" : "http://example.com/blah/", "xmlns:MyD" : "DAV:"})
        self.assertEqual(d["propfind"].data, "")

        self.assertEqual(len(d["propfind"]), 1)
        self.assertEqual(d["propfind"]["prop"].name, "prop")
        self.assertEqual(d["propfind"]["prop"].attrs, {})
        self.assertEqual(d["propfind"]["prop"].data, "")

        self.assertEqual(len(d["propfind"]["prop"]), 1)
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].name, "getlastmodified")
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].attrs, {"xmlns" : "DAV:"})
        self.assertEqual(d["propfind"]["prop"]["getlastmodified"].data, "")

if __name__ == '__main__': # functionality test

    if len(sys.argv) > 1 and sys.argv[1] == "unittest":
        #unittest.main() # strangely, this doesn't work

        suite = unittest.TestLoader().loadTestsFromTestCase(XMLTest)
        unittest.TextTestRunner(verbosity=2).run(suite)
        sys.exit(0)

    p = XMLDict_Parser('<tag1>text</tag1>')
    d = p.builddict()
    print d
    print "Contents of tag1 is: '%s'" % d['tag1'].data
    p = XMLDict_Parser('<group><user>joe</user><user>nick</user><user>john</user></group>')
    d = p.builddict()
    print d
    print 'users are:'
    for u in d['group']['user']:
        print u
#    print d['group']
#    print d['group'].d
    p = XMLDict_Parser('<group><user/><user/><user/></group>')
    d = p.builddict()
    print d
#    print d['group'].d
    p = XMLDict_Parser('<users><joe/><nick/><john/></users>')
    d = p.builddict()
    print d
    if 'joe' in d['users']:
        print 'have no fear, joe is near.'
    if 'george' in d['users']:
        print 'george is evil'
    print 'users are:'
    for u in d['users']:
        print u

