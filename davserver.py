# Copyright (c) 2005.-2006. Ivan Voras <ivoras@gmail.com>
# Released under the Artistic License

from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from threading import Thread, Lock
from SocketServer import ThreadingMixIn
import xmldict
from collection import *
from member import Member
from StringIO import StringIO
import sys,urllib
from xml.sax.saxutils import escape


class DAVRequestHandler(BaseHTTPRequestHandler):
    protocol_version = 'HTTP/1.1'
    server_version = "pandav/0.2"
    all_props = ['name', 'parentname', 'href', 'ishidden', 'isreadonly', 'getcontenttype',
                'contentclass', 'getcontentlanguage', 'creationdate', 'lastaccessed', 'getlastmodified',
                'getcontentlength', 'iscollection', 'isstructureddocument', 'defaultdocument',
                'displayname', 'isroot', 'resourcetype']
    basic_props = ['name', 'getcontenttype', 'getcontentlength', 'creationdate', 'iscollection']

    def do_OPTIONS(self):
        if not(self.server.authHandler.checkAuthentication(self)):
            return

        self.decodeHTTPPath()

        self.send_response(200, DAVRequestHandler.server_version+' reporting for duty.')
        self.send_header('Allow', 'GET, HEAD, POST, PUT, DELETE, OPTIONS, PROPFIND, PROPPATCH, MKCOL')
        self.send_header('Content-length', '0')
        self.send_header('X-Server-Copyright', DAVRequestHandler.server_version+' (c) 2005. Ivan Voras <ivoras@gmail.com>')
        self.send_header('DAV', '1')
        self.send_header('MS-Author-Via', 'DAV')
        self.end_headers()


    def do_PROPFIND(self):

        if not(self.server.authHandler.checkAuthentication(self)):
            return

        self.decodeHTTPPath()

        depth = 'infinity'

        if 'Depth' in self.headers:
            depth = self.headers['Depth'].lower()
        if 'Content-length' in self.headers:
            req = self.rfile.read(int(self.headers['Content-length']))
        else:
            req = self.rfile.read()

        d = xmldict.builddict(req)

        wished_all = False
        if len(d) == 0:
            #wished_props = DAVRequestHandler.all_props
            wished_props = DAVRequestHandler.basic_props
        else:
            if 'allprop' in d['propfind']:
                wished_props = DAVRequestHandler.all_props
                wished_all = True
            else:
                wished_props = []
                for prop in d['propfind']['prop']:
                    wished_props.append(prop)

        path, elem = self.path_elem()
        if not elem:
            #print "path", repr(path), "elem", repr(elem), 'self.path', self.path
            if len(path) >= 1: # it's a non-existing file
                self.send_error(404, 'Not found')
                return
            else:
                elem = self.server.root # fixup root lookups?
                

        if depth != '0' and (not elem or elem.type != Member.M_COLLECTION):
            print "responding with 406 (depth: %s, elem: %s, elem.type: %s)" % (depth, str(elem), str(elem.type))
            self.send_error(406, 'This is not allowed')
            return

        self.send_response(207, 'Multistatus')
        self.send_header('Content-Type', 'text/xml')

        w = BufWriter(self.wfile, False)

        w.write('<?xml version="1.0" encoding="utf-8"?>\n')
        w.write('<D:multistatus xmlns:D="DAV:">\n')

        def write_props_member(w, m):
            w.write('<D:response>\n')
            assert(type(m.virname) == unicode)
            w.write('<D:href>%s</D:href>\n' % urllib.quote(m.virname.encode("utf-8")))

            w.write('<D:propstat>\n') # return known properties
            w.write('<D:status>HTTP/1.1 200 OK</D:status>\n')
            w.write('<D:prop>\n')
            props = m.getProperties()
            returned_props = []
            #print "props for", m.name, repr(props)
            for p in props: # write out properties
                if props[p] == None:
                    w.write('  <D:%s/>\n' % p)
                else:
                    w.write(u'  <D:%s>%s</D:%s>\n' % (p, escape( unicode(props[p]) ), p))
                returned_props.append(p)
            if m.type != Member.M_COLLECTION:
                w.write('  <D:resourcetype/>\n')
            else:
                w.write('  <D:resourcetype><D:collection/></D:resourcetype>\n')
            w.write('</D:prop>\n')
            w.write('</D:propstat>\n')

            if not wished_all and len(returned_props) < len(wished_props): # notify that some properties were not found
                w.write('<D:propstat>\n')
                w.write('<D:status>HTTP/1.1 404 Not found</D:status>\n')
                w.write('<D:prop>\n')
                for wp in wished_props:
                    if not wp in returned_props:
                        w.write('<D:%s/>' % wp)
                w.write('</D:prop>\n')
                w.write('</D:propstat>\n')
            
            w.write('<D:lockdiscovery/>\n<D:supportedlock/>\n')
            w.write('</D:response>\n')

        write_props_member(w, elem)

        if depth == '1':
            for m in elem.getMembers():
                write_props_member(w,m)

        w.write('</D:multistatus>')
       
        self.send_header('Content-Length', str(w.getSize()))
        self.end_headers()
        w.flush()


    def do_GET(self, onlyhead=False):
        ##import pdb;pdb.set_trace()

        if not(self.server.authHandler.checkAuthentication(self)):
            return

        self.decodeHTTPPath()

        path, elem = self.path_elem()
        if not elem:
            self.send_error(404, 'Object not found')
            return

        try:
            props = elem.getProperties()
        except:
            self.send_error(500, "Error retrieving properties")
            return

        #print self.headers

        self.send_response(200, "Ok, here you go")
        if elem.type == Member.M_MEMBER:
            self.send_header("Content-type", props['getcontenttype'])
            self.send_header("Content-length", props['getcontentlength'])
            self.send_header("Last-modified", props['getlastmodified'])
            self.end_headers()

            if not onlyhead:
                elem.sendData(self.wfile)
        else:
#            self.send_header("Content-type", "application/x-collection")
            try:
                ctype = props['getcontenttype']
            except:
                ctype = DirCollection.COLLECTION_MIME_TYPE
            self.send_header("Content-type", ctype)

            if not onlyhead:
                # buffer directory output so we can prepend length
                w = BufWriter(self.wfile, False)
                elem.sendData(w)
                self.send_header('Content-Length', str(w.getSize()))
                self.end_headers()
                w.flush()
            else:
                self.send_header('Content-Length', '0')
                self.end_headers()


    def do_HEAD(self):
        self.do_GET(True) # HEAD should behave like GET, only without contents


    def do_DELETE(self):

        if not(self.server.authHandler.checkAuthentication(self)):
            return

        self.decodeHTTPPath()

        self.send_error(403, 'deletion not allowed')

    def do_MKCOL(self):

        if not(self.server.authHandler.checkAuthentication(self)):
            return

        self.decodeHTTPPath()

        # MKCOL requests with message bodies are not supported at all (RFC4918:9.3.1, code 415)
        if 'Content-length' in self.headers and int(self.headers['Content-length']) > 0:
            req = self.rfile.read(int(self.headers['Content-length']))
            self.send_error(415, "MKCOL message bodies not supported")
            return

        path, elem = self.path_elem_prev()
        print "base elem: %s" % elem

        if not(elem):
            self.send_error(409, "parent doesn't exist")
            return

        segments = self.split_path(self.path)
        print "new segment: %s" % segments[-1]

        try:
            elem.createSubCollection(segments[-1])
        except CollectionExistsError:
            self.send_error(405, 'folder exists')
        except Exception, e:
            print "exception: %s" % str(e)
            self.send_error(500, 'internal exception')
        else:
            self.send_response(201, 'folder created')
            self.send_header('Content-length', '0')
            self.end_headers()

    def do_PUT(self):

        if not(self.server.authHandler.checkAuthentication(self)):
            return

        self.decodeHTTPPath()

        try:
            if 'Content-length' in self.headers:
                size = int(self.headers['Content-length'])
            else:
                size = -1
            path, elem = self.path_elem_prev()
            ename = path[-1]
        except:
            self.send_error(400, 'Cannot parse request')
            return

        try:
            elem.recvMember(self.rfile, ename, size, self)
        except:
            self.send_error(500, 'Cannot save file')
            return

        self.send_response(201, 'Ok, received')
        self.send_header('Content-length', '0')
        self.end_headers()


#    def send_response(self, code, msg=''):
#        """Sends HTTP response line and mandatory headers"""
#        BaseHTTPRequestHandler.send_response(self, code, msg)
#        self.send_header('DAV', '1')


    def decodeHTTPPath(self):
        """Decodes the HTTP path value"""

        # HTTP path is apparently in utf-8 encoding + url quoting
        assert(type(self.path) == str)
        self.path = urllib.unquote(self.path).decode("utf-8")


    def split_path(self, path):
        """Splits path string in form '/dir1/dir2/file' into parts"""
        p = path.split('/')[1:]
        while p and p[-1] in ('','/'):
           p = p[:-1]
           if len(p) > 0:
              p[-1] += '/'
        return p


    def path_elem(self):
        """Returns split path (see split_path()) and Member object of the last element"""
        path = self.split_path(self.path)
        elem = self.server.root
        for e in path:
            elem = elem.findMember(e)
            if elem == None:
                break
        return (path, elem)


    def path_elem_prev(self):
        """Returns split path (see split_path()) and Member object of the next-to-last element"""
        path = self.split_path(self.path)
        elem = self.server.root
        for e in path[:-1]:
            elem = elem.findMember(e)
            if elem == None:
                break
        return (path, elem)

    def logReq(self, text):
        print "---- 8< ----"
        print text
        print "---- >8 ----"
        pass


class BufWriter:
    def __init__(self, w, debug=True):
        self.w = w
        self.buf = StringIO(u'')
        self.debug = debug

    def write(self, s):
        if self.debug:
            if type(s) == unicode:
                sys.stderr.write(s.encode("utf-8"))
            else:
                sys.stderr.write(s)

        if type(s) == unicode:
            self.buf.write(s)
        else:
            self.buf.write( unicode(s, "ascii") ) # assume it's ASCII - TODO: remove this branch
        
    def flush(self):
        self.w.write(self.buf.getvalue().encode('utf-8'))
        self.w.flush()

    def getSize(self):
        return len(self.buf.getvalue().encode('utf-8'))


class HTTPBasicAuthHandler:
    def __init__ (self, realm=""):
        self.realm = realm
        self.users = {
            'admin' : 'adminpass',
            'joeuser' : 'joe'
        }

        print "%d users in auth database for '%s'" % (len(self.users.keys()), self.realm)

    def checkAuthentication (self, reqHandler):
        """
        Checks that the HTTP header contains valid authentication data.
        Returns true if correct authentication was provided, false otherwise.
        """

        #print ""
        #print "====== %s ======" % reqHandler.command
        #print "version: %s; path = %s" % (reqHandler.request_version, reqHandler.path)
        #print reqHandler.headers

        isAuthed = False

        if reqHandler.headers.dict.has_key('authorization'):
            #print "found auth line (%s)" % self.headers.dict['authorization']
            authContent = reqHandler.headers.dict['authorization']
            if authContent.startswith("Basic "):
                import base64
                b64 = authContent[6:]
                (username, password) = base64.decodestring(b64).split(":")
                #print "user: '%s'; pass: '%s'" % (username, password)

                if self.users.has_key(username):
                    if self.users[username] == password:
                        isAuthed = True
                    else:
                        print "wrong password for user '%s'" % username
                else:
                    print "unknown user '%s'" % username
            else:
                print "unknown authorization string '%s'" % authContent
        else:
            print "no auth header provided"
            print reqHandler.headers

        if not(isAuthed):
            # make sure there is no more data waiting to be received:
            if 'Content-length' in reqHandler.headers:
                reqHandler.rfile.read(int(reqHandler.headers['Content-length']))

            #print "no (or bad) auth provided"
            reqHandler.send_response(401)
            reqHandler.send_header('WWW-Authenticate','basic realm="%s"' % self.realm)
            reqHandler.send_header('Content-length', '0')
            reqHandler.end_headers()

        return isAuthed

class DAVServer(ThreadingMixIn, HTTPServer):

    def __init__(self, addr, handler, root):
        HTTPServer.__init__(self, addr, handler)
        self.root = root
        self.authHandler = HTTPBasicAuthHandler("SomeRealm")



