Usage:

$ python server.py /somedir/ortheother port

 (for example:)

$ python server.py /home/joe 8080

To extend PanDAV for serving arbitrary content, you should write two
classes, one a descentant of Collection class, and other of Member
class. Collections represent directories and Members represent files.
For example, see how fsdav.py does it.

PanDAV is licensed under the Artistic License. See COPYRIGHT.txt
