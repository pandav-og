# Copyright (c) 2005.-2006. Ivan Voras <ivoras@gmail.com>
# Released under the Artistic License

from davserver import *
from fsdav import DirCollection, FileMember
import sys

if len(sys.argv) == 3:
    assert(type(sys.argv[1]) == str) # make sure that the argument is in ASCII encoding, as we rely on that
    sharedir = unicode(sys.argv[1])
    serverport = int(sys.argv[2])
else:
    sharedir = u'/'
    serverport = 8080


## Start the server
print "PanDAV (c)2005. Ivan Voras <ivoras@gmail.com>"
print "Serving %s on port %d ..." % (sharedir, serverport)

server_address = ('', serverport)
root = DirCollection(sharedir, u'/')
httpd = DAVServer(server_address, DAVRequestHandler, root)
httpd.serve_forever() # todo: add some control over starting and stopping the server
